package org.lealoureiro.monitor;

import org.apache.log4j.Logger;

import java.util.TreeMap;

public class HostInterface {

    private static Logger log = Logger.getLogger(HostInterface.class);

    private int index;
    private String name;
    private String type;
    private short duplex;
    private long speedBits;
    private volatile int status;
    private TreeMap<Integer, InterfaceOctets> lastMinute;
    private TreeMap<Integer, InterfaceOctets> lastHour;
    private volatile double lastMinuteUse;
    private volatile double lastHourUse;
    private static long counter32MaxValue = 4294967295l;
    private int lastPacketTime;
    private long lastInOctets;
    private long lastOutOctets;
    private int inCounterResetNumber;
    private int outCounterResetNumber;

    public HostInterface(int index) {
        this.index = index;

        this.lastMinute = new TreeMap<Integer, InterfaceOctets>();
        this.lastHour = new TreeMap<Integer, InterfaceOctets>();

        this.lastMinuteUse = 0;
        this.lastHourUse = 0;
        this.status = 2;
        this.lastPacketTime = Integer.MIN_VALUE;
        this.lastInOctets = 0l;
        this.lastOutOctets = 0l;
        this.inCounterResetNumber = 0;
        this.outCounterResetNumber = 0;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return this.index;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return this.name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDuplex(short duplex) {
        this.duplex = duplex;
    }

    public String getType() {
        return this.type;
    }

    public void setSpeed(long speedBits) {
        this.speedBits = speedBits;
    }

    public long getSpeed() {
        return this.speedBits;
    }

    public int getStatus() {
        return this.status;
    }

    public double getLastMinuteUse() {
        return this.lastMinuteUse;
    }

    public double getLastHourUse() {
        return this.lastHourUse;
    }

    public String getStatusDescription() {
        String description = "";
        switch (this.status) {
            case 1:
                description = "up";
                break;
            case 2:
                description = "down";
                break;
            case 3:
                description = "testing";
                break;
            default:
                description = "unknown";
        }

        return description;
    }

    public void addValue(int timestamp, long in, long out) {
        int min = Integer.MIN_VALUE;
        if (this.lastMinute.size() > 0) {
            min = this.lastMinute.firstKey();

        }

        if (this.lastMinute.containsKey(timestamp)) {
            log.warn("Warning: Last minute repeated time value!");
            log.debug("Database values: In: " + this.lastMinute.get(timestamp).getInOctets() + " Out: " + this.lastMinute.get(timestamp).getOutOctets());
            log.debug("New values values: In: " + in + " Out: " + out);
        }

        // register values for more recent time
        if (timestamp > this.lastPacketTime) {
            // check if counter are back to ZERO
            if (in < this.lastInOctets) {
                this.inCounterResetNumber++;
            }
            if (out < this.lastOutOctets) {
                this.outCounterResetNumber++;
            }

            this.lastPacketTime = timestamp;
            this.lastInOctets = in;
            this.lastOutOctets = out;
        }

        in += this.inCounterResetNumber * HostInterface.counter32MaxValue;
        out += this.outCounterResetNumber * HostInterface.counter32MaxValue;

        log.debug("Host time: " + this.lastPacketTime);
        log.debug("Scanned In MB/s: " + (in / 1024 / 1024));
        log.debug("Scanned Out MB/s: " + (out / 1024 / 1024));

        if (timestamp > min) { // check if is not a package outside of time range
            this.lastMinute.put(timestamp, new InterfaceOctets(in, out, timestamp));
        }

        if (this.lastHour.size() > 0) {
            min = this.lastHour.firstKey();
        } else {
            min = Integer.MIN_VALUE;
        }

        if (timestamp > min) { // check if is not a package outside of time range
            this.lastHour.put(timestamp, new InterfaceOctets(in, out, timestamp));
        }

        this.removeOlderValues(timestamp);
    }

    public void calculate() {

        int recent = this.lastMinute.lastKey();
        int old = this.lastMinute.firstKey();

        log.debug("In counter reset number: " + this.inCounterResetNumber);
        log.debug("Out counter reset number: " + this.outCounterResetNumber);

        // calculate delta IN octets for last MINUTE
        long RecentInOctets = this.lastMinute.get(recent).getInOctets();
        long OldInOctets = this.lastMinute.get(old).getInOctets();
        long deltaInOctets = RecentInOctets - OldInOctets;
        log.debug("delta in " + deltaInOctets);
        if (deltaInOctets < 0) {
            log.debug("Warning: " + this.name + " Last Minute In Octets negative value!");
            log.debug("Old time: " + old + " Recent time: " + recent);
            log.debug("Old octets: " + OldInOctets + " Recent octets: " + RecentInOctets);
        }

        // calculate delta OUT octets for last MINUTE
        long RecentOutOctets = this.lastMinute.get(recent).getOutOctets();
        long OldOutOctets = this.lastMinute.get(old).getOutOctets();
        long deltaOutOctets = RecentOutOctets - OldOutOctets;
        log.debug("delta out " + deltaOutOctets);
        if (deltaOutOctets < 0) {
            log.debug("Warning: " + this.name + " Last Minute Out Octets negative value!");
            log.debug("Old time: " + old + " Recent time: " + recent);
            log.debug("Old octets: " + OldOutOctets + " Recent octets: " + RecentOutOctets);
        }

        long deltaTime = recent - old;
        if (this.lastMinute.size() > 1 && this.speedBits != 0) {
            if (this.duplex == InterfaceType.HALF_DUPLEX)
                this.lastMinuteUse = calculateHalfDuplexPercentage(deltaInOctets, deltaOutOctets, deltaTime);
            else {
                this.lastMinuteUse = calculateFullDuplexPercentage(deltaInOctets, deltaOutOctets, deltaTime);
            }
        }

        if (this.lastMinuteUse > 100) {
            log.debug("Warning: Last Minute Usage greater than 100%!");
            for (InterfaceOctets io : this.lastMinute.values()) {
                log.debug("Time: " + io.getTimestamp() + " In: " + io.getInOctets() + " Out: " + io.getOutOctets());
            }
        }
        log.debug("delta time: " + deltaTime);
        log.debug("Total Values in Database: " + this.lastMinute.size());
        log.debug("Last Minute Usage: " + this.lastMinuteUse + "%");


        recent = this.lastHour.lastKey();
        old = this.lastHour.firstKey();

        // calculate delta IN octets for last HOUR
        RecentInOctets = this.lastHour.get(recent).getInOctets();
        OldInOctets = this.lastHour.get(old).getInOctets();
        deltaInOctets = RecentInOctets - OldInOctets;
        log.debug("delta in " + deltaInOctets);
        if (deltaInOctets < 0) {
            log.debug("Warning: " + this.name + " Last Hour In Octets negative value!" + deltaInOctets);
            log.debug("Old time: " + old + " Recent: " + recent);
            log.debug("Old octets: " + OldInOctets + " Recent octets: " + RecentInOctets);
        }

        // calculate delta OUT octets for last HOUR
        RecentOutOctets = this.lastHour.get(recent).getOutOctets();
        OldOutOctets = this.lastHour.get(old).getOutOctets();
        deltaOutOctets = RecentOutOctets - OldOutOctets;
        log.debug("delta out " + deltaOutOctets);
        if (deltaOutOctets < 0) {
            log.debug("Warning: " + this.name + " Last Hour Out Octets negative value!");
            log.debug("Old time: " + old + " Recent: " + recent);
            log.debug("Old octets " + OldOutOctets + " Recent: " + RecentOutOctets);
        }

        deltaTime = recent - old;
        if (this.lastHour.size() > 1 && this.speedBits != 0) {
            this.lastHourUse = ((double) Math.max(deltaInOctets, deltaOutOctets) * 8d * 100d) / ((double) deltaTime * (double) this.speedBits);
        }
        if (this.lastHourUse > 100) log.debug("Warning: Last Hour Usage greater than 100%!");
        log.debug("delta time: " + deltaTime);
        log.debug("Total Values in Database: " + this.lastHour.size());
        log.debug("Last Hour Usage: " + this.lastHourUse + "%");
        log.debug("Operational Status: " + this.getStatusDescription());
        log.debug("Link Speed: " + (this.speedBits / 1000 / 1000) + " Mbits/sec");
    }

    public void removeOlderValues(int timestamp) {

        int min = Integer.MIN_VALUE;
        int i;
        if (this.lastMinute.size() > 0) {
            min = this.lastMinute.firstKey();
        }

        if (timestamp > min && this.lastMinute.size() > 0) {

            i = this.lastMinute.firstKey();

            while ((i + 60) < timestamp) {
                this.lastMinute.remove(i);
                i = this.lastMinute.firstKey();
                if (this.lastMinute.size() == 0) {
                    i = Integer.MIN_VALUE; // safety cycle end;
                }
            }
        }

        if (this.lastHour.size() > 0) {
            min = this.lastHour.firstKey();
        } else {
            min = Integer.MIN_VALUE;
        }

        if (timestamp > min && this.lastHour.size() > 0) {

            i = this.lastHour.firstKey();

            while ((i + 3600) < timestamp) {
                this.lastHour.remove(i);
                i = this.lastHour.firstKey();
                if (this.lastHour.size() == 0) {
                    i = Integer.MIN_VALUE; // safety cycle end;
                }
            }

        }
    }

    private double calculateHalfDuplexPercentage(long deltaInOctets, long deltaOutOctets, long deltaTime) {
        return ((double) (deltaInOctets + deltaOutOctets) * 8d * 100d) / ((double) deltaTime * (double) this.speedBits);
    }

    private double calculateFullDuplexPercentage(long deltaInOctets, long deltaOutOctets, long deltaTime) {
        return ((double) Math.max(deltaInOctets, deltaOutOctets) * 8d * 100d) / ((double) deltaTime * (double) this.speedBits);
    }

}
