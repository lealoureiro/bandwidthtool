package org.lealoureiro.monitor;


import org.apache.log4j.Logger;
import org.snmp4j.mp.MPv3;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.smi.OctetString;

import javax.swing.*;
import java.util.HashMap;

public final class CoreApp {

    private static Logger log = Logger.getLogger(CoreApp.class);
    private HashMap<String, SNMPHost> hosts;

    public CoreApp() {
        this.hosts = new HashMap<String, SNMPHost>();
        USM usm = new USM(SecurityProtocols.getInstance(), new OctetString(MPv3.createLocalEngineID()), 0);
        SecurityModels.getInstance().addSecurityModel(usm);
    }

    public void addHost(String address, int port, String user, String authAlgo, String authPass, String encAlgo, String encPass)
            throws Exception {

        if (this.hosts.containsKey(address + port))
            throw new Exception("Host already in scanning!");

        SNMPHost host = new SNMPHost(address, port, this, user, authAlgo, authPass, encAlgo, encPass);

        host.init();
        host.parseInterfaces();
        HostViewer hv = new HostViewer(address, host.getInterfaceDetails(), host);
        hv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        host.setViewer(hv);
        hv.setVisible(true);
        hv.drawTable();
        host.start();

        this.hosts.put(address + port, host);
        log.info("Total hosts scanning: " + this.hosts.size());
    }

    public void closeAll() {

        for (SNMPHost host : this.hosts.values()) {
            host.getHostViewer().setVisible(false);
            host.getHostViewer().dispose();
            host.close();
        }

        for (SNMPHost host : this.hosts.values()) {
            try {
                host.join();
            } catch (InterruptedException ex) {
                log.error(ex);
            }
        }

        this.hosts.clear();
    }

    public void notifyClose(String key) {
        this.hosts.remove(key);
    }

}
