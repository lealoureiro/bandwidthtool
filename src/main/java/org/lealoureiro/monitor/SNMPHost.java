package org.lealoureiro.monitor;

import org.apache.log4j.Logger;
import org.snmp4j.*;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.*;
import org.snmp4j.smi.*;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.snmp4j.util.DefaultPDUFactory;
import org.snmp4j.util.TableEvent;
import org.snmp4j.util.TableUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SNMPHost extends Thread {

    private static Logger log = Logger.getLogger(SNMPHost.class);

    private String address;
    private int port;
    private String user;
    private String authAlgo;
    private String authPass;
    private String encAlgo;
    private String encPass;
    private TransportMapping transport;
    private Snmp snmp;
    private Address targetAddress;
    private UserTarget target;
    private ArrayList<HostInterface> interfaces;
    private HostViewer hv;
    private volatile int polltime;
    private volatile boolean pollingAlive;
    private CoreApp app;

    public SNMPHost(String address, int port, CoreApp app, String user, String authAlgo, String authPass, String encAlgo, String encPass) {
        this.address = address;
        this.port = port;
        this.polltime = 10;
        this.interfaces = new ArrayList<HostInterface>();
        this.pollingAlive = true;
        this.setName("Monitor " + this.address + ":" + this.port);
        this.app = app;
        this.user = user;
        this.authAlgo = authAlgo;
        this.authPass = authPass;
        this.encAlgo = encAlgo;
        this.encPass = encPass;
    }

    public void init()
            throws IOException {
        this.transport = new DefaultUdpTransportMapping();
        transport.listen();
        this.snmp = new Snmp(transport);


        OID auth = AuthMD5.ID;
        OID enc = PrivDES.ID;

        if (this.authAlgo.equals("SHA")) {
            auth = AuthSHA.ID;
        }

        if (this.encAlgo.equals("AES128")) {
            enc = PrivAES128.ID;
        } else if (this.encAlgo.equals("AES192")) {
            enc = PrivAES192.ID;
        } else if (this.encAlgo.equals("AES256")) {
            enc = PrivAES256.ID;
        }

        this.snmp.getUSM().addUser(new OctetString(this.user),
                new UsmUser(new OctetString(this.user),
                        auth,
                        new OctetString(this.authPass),
                        enc,
                        new OctetString(this.encPass)));

        this.targetAddress = GenericAddress.parse("udp:" + this.address + "/" + this.port);
        target = new UserTarget();
        target.setAddress(targetAddress);
        target.setRetries(2);
        target.setTimeout(1000);
        target.setVersion(SnmpConstants.version3);
        target.setSecurityLevel(SecurityLevel.AUTH_PRIV);
        target.setSecurityName(new OctetString(this.user));
    }

    public void setPollingTime(int time) {
        this.polltime = time;
    }

    public void setViewer(HostViewer hv) {
        this.hv = hv;
    }

    public HostViewer getHostViewer() {
        return this.hv;
    }

    public int getPollingTime() {
        return this.polltime;
    }

    public ArrayList<HostInterface> getInterfaceDetails() {
        return this.interfaces;
    }

    public List<TableEvent> getTableValues(OID oids[]) {
        DefaultPDUFactory factory = new DefaultPDUFactory();
        TableUtils tableUtils = new TableUtils(this.snmp, factory);
        List<TableEvent> events = tableUtils.getTable(target, oids, null, null);

        return events;
    }

    public void parseInterfaces()
            throws Exception {

        InterfaceType types = new InterfaceType();

        OID[] oids = new OID[4];
        oids[0] = new OID("1.3.6.1.2.1.2.2.1.1"); // index
        oids[1] = new OID("1.3.6.1.2.1.2.2.1.2"); // name
        oids[2] = new OID("1.3.6.1.2.1.2.2.1.3"); // interface type
        oids[3] = new OID("1.3.6.1.2.1.2.2.1.5"); // interface speed (bugged)

        List<TableEvent> events = this.getTableValues(oids);

        interfaces = new ArrayList<HostInterface>(events.size());
        int i = 0;
        for (TableEvent event : events) {
            if (event.isError()) {
                throw new Exception("Table Event Error!");
            }

            VariableBinding[] vbs = event.getColumns();

            VariableBinding vb = vbs[0];
            // create new class interface
            HostInterface iface = new HostInterface(Integer.parseInt(vb.getVariable().toString()));
            // set interface name
            vb = vbs[1];
            iface.setName(vb.getVariable().toString());

            // set interface type
            vb = vbs[2];
            iface.setType(types.parseIntType(Integer.parseInt(vb.getVariable().toString())));
            iface.setDuplex(types.getDuplex(Short.parseShort(vb.getVariable().toString())));

            vb = vbs[3];
            iface.setSpeed(Integer.parseInt(vb.getVariable().toString()));
            interfaces.add(iface);
        }

    }

    public void fetchOctets() {

        PDU pdu = new ScopedPDU();
        int limit = this.interfaces.size() + 1;
        for (int i = 1; i < limit; i++) {
            pdu.add(new VariableBinding(new OID(".1.3.6.1.2.1.2.2.1.10." + i)));
            pdu.add(new VariableBinding(new OID(".1.3.6.1.2.1.2.2.1.16." + i)));
            pdu.add(new VariableBinding(new OID(".1.3.6.1.2.1.2.2.1.8." + i)));
        }

        pdu.add(new VariableBinding(new OID(".1.3.6.1.2.1.1.3.0")));
        pdu.setType(PDU.GET);

        try {
            ResponseEvent event = this.snmp.send(pdu, target, transport);
            if (event != null) {
                String formattedDate = event.getResponse().get((this.interfaces.size() * 3)).getVariable().toString();

                double ts = 0;
                String[] dateParts = formattedDate.split(" ");
                if (dateParts.length == 3) {
                    ts += 86400 * Integer.parseInt(dateParts[0]);
                    dateParts = dateParts[2].split(":");
                } else {
                    dateParts = dateParts[0].split(":");
                }

                ts += 3600 * Integer.parseInt(dateParts[0]);
                ts += 60 * Integer.parseInt(dateParts[1]);
                dateParts = dateParts[2].split("\\.");

                ts += Integer.parseInt(dateParts[0]);
                ts += (Double.parseDouble(dateParts[1]) / 100d);

                log.debug("");
                log.debug("Not rounded: " + ts);
                int timestamp = (int) Math.round(ts);
                log.debug("Rounded: " + timestamp);

                limit = this.interfaces.size() * 3;
                for (int i = 0; i < limit; i += 3) {


                    int status = Integer.parseInt(event.getResponse().get((i + 2)).getVariable().toString());

                    HostInterface iface = this.interfaces.get(i / 3);
                    log.debug("");
                    log.debug("Host: " + this.address + " Interface: " + iface.getName());

                    iface.setStatus(status);


                    iface.addValue(timestamp, Long.parseLong(event.getResponse().get((i)).getVariable().toString()), Long.parseLong(event.getResponse().get((i + 1)).getVariable().toString()));

                    this.interfaces.get(i / 3).calculate();
                }
            }
        } catch (Exception ex) {
            log.debug("Failed to get OCTETS for host " + this.address + " error: " + ex);
            ex.printStackTrace();
        }

    }

    public void refreshView() {
        if (this.hv != null) {
            this.hv.drawTable();
        }
    }

    public void close() {
        this.pollingAlive = false;
        try {
            this.transport.close();
        } catch (IOException ex) {
            log.error(ex);
        }
        this.app.notifyClose(this.address + this.port);
    }

    @Override
    public void run() {
        while (this.pollingAlive) {
            this.fetchOctets();
            this.refreshView();
            try {
                Thread.sleep(this.polltime * 1000);
            } catch (InterruptedException ex) {
                log.error(ex);
            }
        }
    }
}
