
package org.lealoureiro.monitor;

import org.apache.log4j.Logger;
import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.LayoutStyle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.InetAddress;


public class AddHostViewer extends JFrame {

    private static Logger log = Logger.getLogger(AddHostViewer.class);
    private CoreApp app;
    private JTextField addressBox;
    private JTextField addressBox1;
    private JComboBox authAlgoBox;
    private JTextField authBox;
    private JComboBox encAlgoBox;
    private JTextArea encPassBox;
    private JLabel errorMessage;
    private JTextField userBox1;

    public AddHostViewer(CoreApp app) {
        this.app = app;
        initComponents();
        this.errorMessage.setVisible(false);

    }

    private void initComponents() {

        JLabel jLabel1 = new JLabel();
        addressBox = new JTextField();
        JLabel jLabel2 = new JLabel();
        JLabel jLabel3 = new JLabel();
        addressBox1 = new JTextField();
        JButton addHostButton = new JButton();
        errorMessage = new JLabel();
        JLabel jLabel4 = new JLabel();
        JLabel jLabel5 = new JLabel();
        JLabel jLabel6 = new JLabel();
        authBox = new JTextField();
        userBox1 = new JTextField();
        authAlgoBox = new JComboBox();
        encAlgoBox = new JComboBox();
        JScrollPane jScrollPane1 = new JScrollPane();
        encPassBox = new JTextArea();

        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
        jLabel1.setText("Add host");

        addressBox.setFont(new Font("Trebuchet MS", 0, 13));

        jLabel2.setFont(new Font("Trebuchet MS", 0, 13));
        jLabel2.setText("Address:");

        jLabel3.setFont(new Font("Trebuchet MS", 0, 13));
        jLabel3.setText("SNMP Port:");

        addressBox1.setFont(new Font("Trebuchet MS", 0, 13));

        addHostButton.setFont(new Font("Trebuchet MS", 0, 13));
        addHostButton.setText("Add");
        addHostButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                addHostButtonActionPerformed(evt);
            }
        });

        errorMessage.setForeground(new Color(255, 0, 0));
        errorMessage.setText("jLabel4");

        jLabel4.setFont(new Font("Trebuchet MS", 0, 13));
        jLabel4.setText("User:");

        jLabel5.setFont(new Font("Trebuchet MS", 0, 13));
        jLabel5.setText("Authentication Password:");

        jLabel6.setFont(new Font("Trebuchet MS", 0, 13));
        jLabel6.setText("Encryption Password:");

        authBox.setFont(new Font("Trebuchet MS", 0, 13));

        userBox1.setFont(new Font("Trebuchet MS", 0, 13));

        authAlgoBox.setFont(new Font("Trebuchet MS", 0, 13));
        authAlgoBox.setModel(new DefaultComboBoxModel(new String[]{"MD5", "SHA"}));

        encAlgoBox.setFont(new Font("Trebuchet MS", 0, 13));
        encAlgoBox.setModel(new DefaultComboBoxModel(new String[]{"DES", "AES128", "AES192", "AES256"}));

        encPassBox.setColumns(20);
        encPassBox.setRows(5);
        jScrollPane1.setViewportView(encPassBox);

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.LEADING)
                        .add(layout.createSequentialGroup()
                                .addContainerGap()
                                .add(layout.createParallelGroup(GroupLayout.LEADING)
                                        .add(jLabel1)
                                        .add(layout.createSequentialGroup()
                                                .add(layout.createParallelGroup(GroupLayout.LEADING)
                                                        .add(jLabel2)
                                                        .add(jLabel3))
                                                .add(18, 18, 18)
                                                .add(layout.createParallelGroup(GroupLayout.LEADING)
                                                        .add(addressBox1, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                                                        .add(addressBox, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)))
                                        .add(GroupLayout.TRAILING, layout.createSequentialGroup()
                                                .add(layout.createParallelGroup(GroupLayout.LEADING)
                                                        .add(jLabel4)
                                                        .add(jLabel5)
                                                        .add(jLabel6))
                                                .add(14, 14, 14)
                                                .add(layout.createParallelGroup(GroupLayout.LEADING)
                                                        .add(jScrollPane1, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE)
                                                        .add(layout.createParallelGroup(GroupLayout.TRAILING, false)
                                                                .add(GroupLayout.LEADING, authBox)
                                                                .add(GroupLayout.LEADING, userBox1, GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)))
                                                .addPreferredGap(LayoutStyle.RELATED, 53, Short.MAX_VALUE)
                                                .add(layout.createParallelGroup(GroupLayout.LEADING)
                                                        .add(authAlgoBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                                        .add(encAlgoBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .add(23, 23, 23))
                                        .add(GroupLayout.TRAILING, addHostButton))
                                .addContainerGap())
                        .add(layout.createSequentialGroup()
                                .add(12, 12, 12)
                                .add(errorMessage, GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                                .add(118, 118, 118))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.LEADING)
                        .add(GroupLayout.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .add(jLabel1)
                                .add(18, 18, 18)
                                .add(layout.createParallelGroup(GroupLayout.BASELINE)
                                        .add(jLabel2)
                                        .add(addressBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.RELATED)
                                .add(layout.createParallelGroup(GroupLayout.BASELINE)
                                        .add(addressBox1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .add(jLabel3))
                                .add(46, 46, 46)
                                .add(layout.createParallelGroup(GroupLayout.BASELINE)
                                        .add(jLabel4)
                                        .add(userBox1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(LayoutStyle.UNRELATED)
                                .add(layout.createParallelGroup(GroupLayout.BASELINE)
                                        .add(jLabel5)
                                        .add(authBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .add(authAlgoBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                .add(26, 26, 26)
                                .add(layout.createParallelGroup(GroupLayout.LEADING)
                                        .add(jLabel6)
                                        .add(encAlgoBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .add(jScrollPane1, GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE))
                                .addPreferredGap(LayoutStyle.RELATED)
                                .add(errorMessage)
                                .add(22, 22, 22)
                                .add(addHostButton))
        );

        pack();
    }

    private void addHostButtonActionPerformed(ActionEvent evt) {

        try {
            InetAddress.getAllByName(this.addressBox.getText());
            int port = Integer.parseInt(this.addressBox1.getText());

            this.app.addHost(this.addressBox.getText(),
                    port,
                    this.userBox1.getText(),
                    this.authAlgoBox.getSelectedItem().toString(),
                    this.authBox.getText(),
                    this.encAlgoBox.getSelectedItem().toString(),
                    this.encPassBox.getText());

            this.errorMessage.setVisible(false);
        } catch (Exception ex) {
            this.errorMessage.setText("error: " + ex);
            this.errorMessage.setVisible(true);
        }


    }

    private void formWindowClosed(WindowEvent evt) {
        log.info("Closing application...");
        this.app.closeAll();
        log.info("Application closed");
    }



}
