package org.lealoureiro.monitor;

import org.apache.log4j.Logger;
import org.jdesktop.layout.GroupLayout;
import org.jdesktop.layout.LayoutStyle;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;


public class HostViewer extends JFrame {

    private static Logger log = Logger.getLogger(HostViewer.class);
    private ArrayList<HostInterface> interfaces;
    private SNMPHost host;
    private JLabel errorMessage;
    private JTable jTable1;
    private JTextField pollingTimeBox;
    private JLabel hostnameLabel;


    public HostViewer(String hostname, ArrayList<HostInterface> ifaces, SNMPHost host) {
        this.interfaces = ifaces;
        this.host = host;
        initComponents();
        this.hostnameLabel.setText(hostname);
        this.pollingTimeBox.setText("" + this.host.getPollingTime());
        this.errorMessage.setVisible(false);
    }

    public void drawTable() {
        this.jTable1.repaint();
    }


    private void initComponents() {

        JLabel jLabel1 = new JLabel();
        hostnameLabel = new JLabel();
        JScrollPane jScrollPane1 = new JScrollPane();
        jTable1 = new JTable();
        pollingTimeBox = new JTextField();
        JButton setPollingTime = new JButton();
        JLabel jLabel2 = new JLabel();
        JLabel jLabel3 = new JLabel();
        errorMessage = new JLabel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosed(WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jLabel1.setFont(new Font("Trebuchet MS", 1, 14));
        jLabel1.setText("Host:");

        hostnameLabel.setText("host name");

        jTable1.setModel(new InterfacesTableModel(this.interfaces));
        jScrollPane1.setViewportView(jTable1);

        pollingTimeBox.setText("jTextField1");

        setPollingTime.setText("Set");
        setPollingTime.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                setPollingTimeActionPerformed(evt);
            }
        });

        jLabel2.setText("Polling Time (sec):");

        jLabel3.setText("(1-10)");

        errorMessage.setForeground(new Color(255, 0, 0));
        errorMessage.setText("Invalid Value!");

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.LEADING)
                        .add(layout.createSequentialGroup()
                                .addContainerGap()
                                .add(layout.createParallelGroup(GroupLayout.LEADING)
                                        .add(jScrollPane1, GroupLayout.DEFAULT_SIZE, 581, Short.MAX_VALUE)
                                        .add(layout.createSequentialGroup()
                                                .add(jLabel1)
                                                .addPreferredGap(LayoutStyle.RELATED)
                                                .add(hostnameLabel))
                                        .add(layout.createSequentialGroup()
                                                .add(jLabel2)
                                                .addPreferredGap(LayoutStyle.RELATED)
                                                .add(pollingTimeBox, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(LayoutStyle.RELATED)
                                                .add(jLabel3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                                                .add(1, 1, 1)
                                                .add(setPollingTime)
                                                .addPreferredGap(LayoutStyle.RELATED)
                                                .add(errorMessage, GroupLayout.PREFERRED_SIZE, 212, GroupLayout.PREFERRED_SIZE)))
                                .addContainerGap())
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.LEADING)
                        .add(layout.createSequentialGroup()
                                .addContainerGap()
                                .add(layout.createParallelGroup(GroupLayout.BASELINE)
                                        .add(jLabel1)
                                        .add(hostnameLabel))
                                .add(18, 18, 18)
                                .add(jScrollPane1, GroupLayout.PREFERRED_SIZE, 204, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.UNRELATED)
                                .add(layout.createParallelGroup(GroupLayout.BASELINE)
                                        .add(jLabel2)
                                        .add(pollingTimeBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                        .add(jLabel3)
                                        .add(setPollingTime)
                                        .add(errorMessage))
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }

    private void formWindowClosed(WindowEvent evt) {
        this.host.close();
        log.info("Window closed");
    }

    private void setPollingTimeActionPerformed(ActionEvent evt) {

        try {
            int value = Integer.parseInt(this.pollingTimeBox.getText());
            if (value > 10 || value < 1) throw new Exception();
            this.host.setPollingTime(value);
            this.errorMessage.setVisible(false);
        } catch (Exception ex) {
            this.errorMessage.setVisible(true);
        }
    }


}
