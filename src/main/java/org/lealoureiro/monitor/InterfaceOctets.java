package org.lealoureiro.monitor;


public class InterfaceOctets {

    private long inOctets;
    private long outOctets;
    private int timestamp;

    public InterfaceOctets(long inOctets, long outOctets, int timestamp) {
        this.inOctets = inOctets;
        this.outOctets = outOctets;
        this.timestamp = timestamp;
    }

    public long getInOctets() {
        return this.inOctets;
    }

    public long getOutOctets() {
        return this.outOctets;
    }

    public int getTimestamp() {
        return this.timestamp;
    }

    public void setInOctets(long octets) {
        this.inOctets = octets;
    }

    public void setOutOctets(long octets) {
        this.outOctets = octets;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}
