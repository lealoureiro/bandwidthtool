/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.lealoureiro.monitor;

import org.apache.log4j.Logger;

import javax.swing.*;


public class ManagementTool {

    private static Logger log = Logger.getLogger(ManagementTool.class);

    public static void main(String[] args) {

        log.info("Starting SR Management Tool v1.0.0");
        CoreApp app = new CoreApp();
        AddHostViewer mainView = new AddHostViewer(app);
        mainView.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        mainView.setVisible(true);
    }
}
