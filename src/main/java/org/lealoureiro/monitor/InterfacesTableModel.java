
package org.lealoureiro.monitor;

import javax.swing.table.AbstractTableModel;
import java.text.DecimalFormat;
import java.util.ArrayList;


public class InterfacesTableModel extends AbstractTableModel {

    private ArrayList<HostInterface> interfaces;
    private String[] columns;
    private DecimalFormat dc;

    public InterfacesTableModel(ArrayList<HostInterface> interfaces) {
        this.interfaces = interfaces;
        this.columns = new String[]{"Index", "Type", "Name", "Speed", "Status", "%use 1 minute", "%use 1 Hour"};
        this.dc = new DecimalFormat("#.##");
    }

    public String getColumnName(int col) {
        return columns[col];
    }

    public int getRowCount() {
        return this.interfaces.size();
    }

    public int getColumnCount() {
        return this.columns.length;
    }

    public Object getValueAt(int row, int col) {
        String data = "";

        switch (col) {
            case 0:
                data = "" + this.interfaces.get(row).getIndex();
                break;

            case 1:
                data = this.interfaces.get(row).getType();
                break;

            case 2:
                data = "" + this.interfaces.get(row).getName();
                break;

            case 3:
                data = "" + (this.interfaces.get(row).getSpeed() / 1000 / 1000) + " Mbps";
                break;
            case 4:
                data = "" + this.interfaces.get(row).getStatusDescription();
                break;

            case 5:
                data = this.dc.format(this.interfaces.get(row).getLastMinuteUse());
                break;

            case 6:
                data = this.dc.format(this.interfaces.get(row).getLastHourUse());
                break;
        }

        return data;
    }
}
